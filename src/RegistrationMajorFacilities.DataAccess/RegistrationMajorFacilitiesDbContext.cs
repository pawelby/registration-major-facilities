﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using RegistrationMajorFacilities.Model;

namespace RegistrationMajorFacilities.DataAccess
{
    public class RegistrationMajorFacilitiesDbContext : DbContext
    {
        public RegistrationMajorFacilitiesDbContext() : base("RegistrationMajorFacilities")
        {
        }

        public DbSet<FixedAsset> FixedAssets { get; set; }

        public DbSet<BranchOffice> BranchOffices { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
