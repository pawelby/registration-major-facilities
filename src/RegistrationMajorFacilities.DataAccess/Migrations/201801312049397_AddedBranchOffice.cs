namespace RegistrationMajorFacilities.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBranchOffice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BranchOffice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        City = c.String(),
                        Country = c.String(),
                        Latitude = c.String(),
                        Longitude = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.FixedAsset", "BranchOfficeId", c => c.Int());
            CreateIndex("dbo.FixedAsset", "BranchOfficeId");
            AddForeignKey("dbo.FixedAsset", "BranchOfficeId", "dbo.BranchOffice", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FixedAsset", "BranchOfficeId", "dbo.BranchOffice");
            DropIndex("dbo.FixedAsset", new[] { "BranchOfficeId" });
            DropColumn("dbo.FixedAsset", "BranchOfficeId");
            DropTable("dbo.BranchOffice");
        }
    }
}
