namespace RegistrationMajorFacilities.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FixedAssets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Number = c.String(nullable: false, maxLength: 20),
                        Count = c.Int(),
                        PassportNumber = c.Int(),
                        ReleaseDate = c.DateTime(),
                        CommissioningDate = c.DateTime(),
                        ReceiptDate = c.DateTime(),
                        DepreciationSum = c.Double(),
                        UsefulLife = c.Double(nullable: false),
                        UsageBeforeReceipt = c.Double(nullable: false),
                        UsageAfterReceipt = c.Double(),
                        InitialCost = c.Double(),
                        AmortizationForReportingPeriod = c.Double(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FixedAssets");
        }
    }
}
