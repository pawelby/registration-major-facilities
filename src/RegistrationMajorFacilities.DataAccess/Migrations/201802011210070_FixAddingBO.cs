namespace RegistrationMajorFacilities.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixAddingBO : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FixedAsset", "BranchOfficeId", "dbo.BranchOffice");
            DropIndex("dbo.FixedAsset", new[] { "BranchOfficeId" });
            AlterColumn("dbo.FixedAsset", "BranchOfficeId", c => c.Int());
            CreateIndex("dbo.FixedAsset", "BranchOfficeId");
            AddForeignKey("dbo.FixedAsset", "BranchOfficeId", "dbo.BranchOffice", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FixedAsset", "BranchOfficeId", "dbo.BranchOffice");
            DropIndex("dbo.FixedAsset", new[] { "BranchOfficeId" });
            AlterColumn("dbo.FixedAsset", "BranchOfficeId", c => c.Int(nullable: false));
            CreateIndex("dbo.FixedAsset", "BranchOfficeId");
            AddForeignKey("dbo.FixedAsset", "BranchOfficeId", "dbo.BranchOffice", "Id", cascadeDelete: true);
        }
    }
}
