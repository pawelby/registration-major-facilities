using RegistrationMajorFacilities.Model;
using System.Data.Entity.Migrations;

namespace RegistrationMajorFacilities.DataAccess.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<RegistrationMajorFacilities.DataAccess.RegistrationMajorFacilitiesDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RegistrationMajorFacilities.DataAccess.RegistrationMajorFacilitiesDbContext context)
        {
            context.FixedAssets.AddOrUpdate(
                f => f.Id, 
                new FixedAsset { Name = "����", Number = "A1"},
                new FixedAsset { Name = "��������� �������", Number = "M1"}
                );

            context.BranchOffices.AddOrUpdate(
                b => b.Id,
                new BranchOffice { Name = "������� ����", City = "�����", Latitude = "53.926288", Longitude = "27.597289", Country = "��������" },
                new BranchOffice { Name = "���������� ����", City = "������", Latitude = "55.753215", Longitude = "37.622504", Country = "��" },
                new BranchOffice { Name = "����� - ������������� ����", City = "����� - ���������", Latitude = "59.939095", Longitude = "30.315868", Country = "��" },
                new BranchOffice { Name = "������ ����", City = "����", Latitude = "54.989342", Longitude = "73.368212", Country = "��" },
                new BranchOffice { Name = "��������������� ����", City = "����������", Latitude = "54.707390", Longitude = "20.507307", Country = "��" },
                new BranchOffice { Name = "������������ ����", City = "����������", Latitude = "56.010563", Longitude = "92.852572", Country = "��" },
                new BranchOffice { Name = "������������� ����", City = "�����������", Latitude = "55.030199", Longitude = "82.920430", Country = "��" },
                new BranchOffice { Name = "��������� ����", City = "������", Latitude = "53.195538", Longitude = "50.101783", Country = "��" },
                new BranchOffice { Name = "��������� ����", City = "������", Latitude = "57.153033", Longitude = "65.534328", Country = "��" },
                new BranchOffice { Name = "����������� ����", City = "������", Latitude = "43.238293", Longitude = "76.945465", Country = "���������" }
            );
        }
    }
}
