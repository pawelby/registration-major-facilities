namespace RegistrationMajorFacilities.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SingleNameDb : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.FixedAssets", newName: "FixedAsset");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.FixedAsset", newName: "FixedAssets");
        }
    }
}
