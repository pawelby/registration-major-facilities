﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RegistrationMajorFacilities.Model
{
    public class FixedAsset
    {
        public int Id { get; set; }

        /// <summary>
        /// Наименование.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Инвентарный номер.
        /// </summary>
        [Required]
        [StringLength(20)]
        public string Number { get; set; }

        /// <summary>
        /// Количество в ИК.
        /// </summary>
        public int? Count { get; set; }

        /// <summary>
        /// Паспортный номер.
        /// </summary>
        public int? PassportNumber { get; set; }
        
        /// <summary>
        /// Дата выпуска.
        /// </summary>
        public DateTime? ReleaseDate { get; set; } 

        /// <summary>
        /// Дата ввода в эксплуатацию.
        /// </summary>
        public DateTime? CommissioningDate { get; set; } 

        /// <summary>
        /// Дата поступления.
        /// </summary>
        public DateTime? ReceiptDate { get; set; } 

        /// <summary>
        /// Сумма износа.
        /// </summary>
        public double? DepreciationSum { get; set; }
        
        /// <summary>
        /// Срок полезного использования.
        /// </summary>
        public double UsefulLife { get; set; }

        /// <summary>
        /// Срок использования до поступления.
        /// </summary>
        public double UsageBeforeReceipt { get; set; }
        
        /// <summary>
        /// Срок использования после поступления.
        /// </summary>
        public double? UsageAfterReceipt { get; set; }

        /// <summary>
        /// Первоначальная стоимость.
        /// </summary>
        public double? InitialCost { get; set; }
        
        /// <summary>
        /// Амортизация за отчетный период.
        /// </summary>
        public double? AmortizationForReportingPeriod { get; set; } 

        public int? BranchOfficeId { get; set; }

        public BranchOffice BranchOffice { get; set; }
    }
}
