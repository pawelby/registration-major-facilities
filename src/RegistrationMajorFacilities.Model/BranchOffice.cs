﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RegistrationMajorFacilities.Model
{
    public class BranchOffice
    {
        public BranchOffice()
        {
            FixedAssets = new Collection<FixedAsset>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public ICollection<FixedAsset> FixedAssets { get; set; }
    }
}
