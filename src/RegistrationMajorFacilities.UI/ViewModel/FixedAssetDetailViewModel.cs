﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using RegistrationMajorFacilities.Model;
using RegistrationMajorFacilities.UI.Data.Lookups;
using RegistrationMajorFacilities.UI.Data.Repositories.Interfaces;
using RegistrationMajorFacilities.UI.Events;
using RegistrationMajorFacilities.UI.Events.EventArgs;
using RegistrationMajorFacilities.UI.View.Services;
using RegistrationMajorFacilities.UI.Wrappers;

namespace RegistrationMajorFacilities.UI.ViewModel
{
    public class FixedAssetDetailViewModel : ViewModelBase, IFixedAssetDetailViewModel
    {
        private IRegistrationRepository _registrationRepository;
        private FixedAssetWrapper _fixedAsset;
        private IEventAggregator _eventAggregator;
        private bool _hasChanges;
        private IMessageDialogService _messageDialogService;
        private IBranchOfficeLookupDataService _branchOfficeLookupDataService;

        public FixedAssetWrapper FixedAsset
        {
            get { return _fixedAsset; }
            private set
            {
                _fixedAsset = value;
                OnPropertyChanged();
            }
        }        

        public bool HasChanges
        {
            get { return _hasChanges; }
            set
            {
                if (_hasChanges != value)
                {
                    _hasChanges = value;
                    OnPropertyChanged();
                    ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();
                }
            }
        }
        
        public ICommand SaveCommand { get; }

        public ICommand DeleteCommand { get; }

        public ObservableCollection<LookupItem> BranchOffices { get; }

        public FixedAssetDetailViewModel(IRegistrationRepository registrationRepository,
            IEventAggregator eventAggregator,
            IMessageDialogService messageDialogService,
            IBranchOfficeLookupDataService branchOfficeLookupDataService)
        {
            _registrationRepository = registrationRepository;
            _eventAggregator = eventAggregator;
            _messageDialogService = messageDialogService;
            _branchOfficeLookupDataService = branchOfficeLookupDataService;
            
            SaveCommand = new DelegateCommand(OnSaveExecute, OnSaveCanExecute);
            DeleteCommand = new DelegateCommand(OnDeleteExecute);

            BranchOffices = new ObservableCollection<LookupItem>();
        }

        public async Task LoadAsync(int? fixedAssetId)
        {
            var fixedAsset = fixedAssetId.HasValue
                ? await _registrationRepository.GetByIdAsync(fixedAssetId.Value)
                : CreateNewFixedAsset();

            InitializeFixedAsset(fixedAsset);

            await LoadBranchOfficesAsync();
        }

        private void InitializeFixedAsset(FixedAsset fixedAsset)
        {
            FixedAsset = new FixedAssetWrapper(fixedAsset);
            FixedAsset.PropertyChanged += (s, e) =>
            {
                if (!HasChanges)
                {
                    HasChanges = _registrationRepository.HasChanges();
                }

                if (e.PropertyName == nameof(FixedAsset.HasErrors))
                {
                    ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();
                }
            };

            ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();

            if (FixedAsset.Id == 0)
            {
                // to trigger validation
                FixedAsset.Name = string.Empty;
                FixedAsset.Number = string.Empty;
            }
        }

        private async Task LoadBranchOfficesAsync()
        {
            BranchOffices.Clear();
            var lookup = await _branchOfficeLookupDataService.GetBranchOfficeLookupAsync();
            foreach (var lookupItem in lookup)
            {
                BranchOffices.Add(lookupItem);
            }
        }

        private FixedAsset CreateNewFixedAsset()
        {
            var fixedAsset = new FixedAsset();

            _registrationRepository.Add(fixedAsset);

            return fixedAsset;
        }

        private bool OnSaveCanExecute()
        {
            return FixedAsset != null && !FixedAsset.HasErrors && HasChanges;
        }

        private async void OnSaveExecute()
        {
            await _registrationRepository.SaveAsync();
            HasChanges = _registrationRepository.HasChanges();
            _eventAggregator.GetEvent<AfterFixedAssetSaveEvent>().Publish(new AfterFixedAssetSavedEventArgs
            {
                Id = FixedAsset.Id,
                DisplayMember = FixedAsset.Name
            });
        }

        private async void OnDeleteExecute()
        {
            var result = _messageDialogService.ShowOkCancelDialog($"Вы точно хотите удалить ОС {FixedAsset.Name}?", "Вопрос");
            if (result == MessageDialogResult.OK)
            {
                _registrationRepository.Remove(FixedAsset.Model);
                await _registrationRepository.SaveAsync();
                _eventAggregator.GetEvent<AfterFixedAssetDeletedEvent>().Publish(FixedAsset.Id);
            }
        }
    }
}
