﻿using Prism.Commands;
using Prism.Events;
using RegistrationMajorFacilities.UI.Events;
using System.Windows.Input;

namespace RegistrationMajorFacilities.UI.ViewModel
{
    public class NavigationItemViewModel : ViewModelBase
    {
        private string _displayMember;
        private IEventAggregator _eventAggregator;

        public int Id { get; }

        public string DisplayMember
        {
            get { return _displayMember; }
            set
            {
                _displayMember = value; 
                OnPropertyChanged();
            }
        }

        public ICommand OpenFixedAssetDetailViewCommand { get; }

        public NavigationItemViewModel(int id, string displayMember, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            Id = id;
            DisplayMember = displayMember;
            OpenFixedAssetDetailViewCommand = new DelegateCommand(OnOpenFixedAssetDetailView);
        }

        private void OnOpenFixedAssetDetailView()
        {
            _eventAggregator.GetEvent<OpenFixedAssetDetailViewEvent>()
                        .Publish(Id);
        }
    }
}
