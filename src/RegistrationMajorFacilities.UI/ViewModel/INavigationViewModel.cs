﻿using System.Threading.Tasks;

namespace RegistrationMajorFacilities.UI.ViewModel
{
    public interface INavigationViewModel
    {
        Task LoadAsync();
    }
}