﻿using System.Threading.Tasks;

namespace RegistrationMajorFacilities.UI.ViewModel
{
    public interface IFixedAssetDetailViewModel
    {
        Task LoadAsync(int? fixedAssetId);

        bool HasChanges { get; }
    }
}