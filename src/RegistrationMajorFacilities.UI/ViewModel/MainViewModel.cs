﻿using Prism.Commands;
using Prism.Events;
using RegistrationMajorFacilities.UI.Events;
using RegistrationMajorFacilities.UI.View.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RegistrationMajorFacilities.UI.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private IEventAggregator _eventAggregator;
        private Func<IFixedAssetDetailViewModel> _fixedAssetDetailViewModelCreator;
        private IFixedAssetDetailViewModel _fixedAssetDetailViewModel;
        private IMessageDialogService _messageDialogService;

        public INavigationViewModel NavigationViewModel { get; }

        public ICommand CreateNewFixedAssetCommand { get; }

        public IFixedAssetDetailViewModel FixedAssetDetailViewModel
        {
            get { return _fixedAssetDetailViewModel; }
            private set
            {
                _fixedAssetDetailViewModel = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel(INavigationViewModel navigationViewModel,
            Func<IFixedAssetDetailViewModel> fixedAssetDetailViewModelCreator,
            IEventAggregator eventAggregator,
            IMessageDialogService messageDialogService)
        {
            _fixedAssetDetailViewModelCreator = fixedAssetDetailViewModelCreator;
            _eventAggregator = eventAggregator;
            _messageDialogService = messageDialogService;

            _eventAggregator.GetEvent<OpenFixedAssetDetailViewEvent>()
                .Subscribe(OnOpenFixedAssetDetailView);
            _eventAggregator.GetEvent<AfterFixedAssetDeletedEvent>()
                .Subscribe(AfterFixedAssetDeleted);

            CreateNewFixedAssetCommand = new DelegateCommand(OnCreateNewFixedAssetExecute);

            NavigationViewModel = navigationViewModel;
        }

        private void AfterFixedAssetDeleted(int fixedAssetId)
        {
            FixedAssetDetailViewModel = null;
        }

        public async Task LoadAsync()
        {
            await NavigationViewModel.LoadAsync();
        }

        private async void OnOpenFixedAssetDetailView(int? fixedAssetId)
        {
            if (FixedAssetDetailViewModel != null && FixedAssetDetailViewModel.HasChanges)
            {
                var result = _messageDialogService.ShowOkCancelDialog("У вас несохраненные изменения. Перейти все равно?", "Вопрос");

                if (result == MessageDialogResult.Cancel)
                {
                    return;
                }
            }

            FixedAssetDetailViewModel = _fixedAssetDetailViewModelCreator();
            await FixedAssetDetailViewModel.LoadAsync(fixedAssetId);
        }

        private void OnCreateNewFixedAssetExecute()
        {
            OnOpenFixedAssetDetailView(null);
        }
    }
}
