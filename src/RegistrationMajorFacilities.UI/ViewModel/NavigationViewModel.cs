﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Prism.Events;
using RegistrationMajorFacilities.UI.Data.Lookups.Interfaces;
using RegistrationMajorFacilities.UI.Events;
using RegistrationMajorFacilities.UI.Events.EventArgs;

namespace RegistrationMajorFacilities.UI.ViewModel
{
    public class NavigationViewModel : ViewModelBase, INavigationViewModel
    {
        private IFixedAssetLookupDataService _fixedAssetLookupDataService;
        private IEventAggregator _eventAggregator;

        public ObservableCollection<NavigationItemViewModel> FixedAssets { get; }
        
        public NavigationViewModel(IFixedAssetLookupDataService fixedAssetLookupDataService,
            IEventAggregator eventAggregator)
        {
            _fixedAssetLookupDataService = fixedAssetLookupDataService;
            _eventAggregator = eventAggregator;
            FixedAssets = new ObservableCollection<NavigationItemViewModel>();
            _eventAggregator.GetEvent<AfterFixedAssetSaveEvent>().Subscribe(AfterFixedAssetSaved);
            _eventAggregator.GetEvent<AfterFixedAssetDeletedEvent>().Subscribe(AfterFixedAssetDeleted);
        }

        private void AfterFixedAssetDeleted(int fixedAssetId)
        {
            var fixedAsset = FixedAssets.SingleOrDefault(x => x.Id == fixedAssetId);
            if (fixedAsset != null)
            {
                FixedAssets.Remove(fixedAsset);
            }
        }

        private void AfterFixedAssetSaved(AfterFixedAssetSavedEventArgs obj)
        {
            var lookupItem = FixedAssets.SingleOrDefault(x => x.Id == obj.Id);
            if (lookupItem == null)
            {
                FixedAssets.Add(new NavigationItemViewModel(obj.Id, obj.DisplayMember, _eventAggregator));
            }
            else
            {
                lookupItem.DisplayMember = obj.DisplayMember;
            }
        }

        public async Task LoadAsync()
        {
            var lookup = await _fixedAssetLookupDataService.GetFixedAssetLookupAsync();
            FixedAssets.Clear();

            foreach (var item in lookup)
            {
                FixedAssets.Add(new NavigationItemViewModel(item.Id, item.DisplayMember, _eventAggregator));
            }
        }
    }
}
