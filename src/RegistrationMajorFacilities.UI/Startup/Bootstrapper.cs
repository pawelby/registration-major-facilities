﻿using Autofac;
using Prism.Events;
using RegistrationMajorFacilities.DataAccess;
using RegistrationMajorFacilities.UI.Data.Lookups;
using RegistrationMajorFacilities.UI.Data.Repositories;
using RegistrationMajorFacilities.UI.Data.Repositories.Interfaces;
using RegistrationMajorFacilities.UI.View.Services;
using RegistrationMajorFacilities.UI.ViewModel;

namespace RegistrationMajorFacilities.UI.Startup
{
    public class Bootstrapper
    {
        public IContainer Bootstrap()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();

            builder.RegisterType<RegistrationMajorFacilitiesDbContext>().AsSelf();

            builder.RegisterType<MainViewModel>().AsSelf();
            builder.RegisterType<MainWindow>().AsSelf();

            builder.RegisterType<MessageDialogService>().As<IMessageDialogService>();

            builder.RegisterType<NavigationViewModel>().As<INavigationViewModel>();
            builder.RegisterType<FixedAssetDetailViewModel>().As<IFixedAssetDetailViewModel>();


            builder.RegisterType<RegistrationRepository>().As<IRegistrationRepository>();

            builder.RegisterType<LookupDataService>().AsImplementedInterfaces();

            return builder.Build();
        }
    }
}
