﻿using RegistrationMajorFacilities.UI.ViewModel;
using System;
using System.Windows;

namespace RegistrationMajorFacilities.UI
{
    public partial class MainWindow : Window
    {
        private MainViewModel _mainViewModel;
            
        public MainWindow(MainViewModel viewModel)
        {
            InitializeComponent();
            _mainViewModel = viewModel;
            DataContext = _mainViewModel;
            Loaded += MainWindow_Loaded;
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            await _mainViewModel.LoadAsync();
        }
    }
}
