﻿using System;
using System.Collections.Generic;
using RegistrationMajorFacilities.Model;

namespace RegistrationMajorFacilities.UI.Wrappers
{
    public class FixedAssetWrapper : ModelWrapper<FixedAsset>
    {
        public FixedAssetWrapper(FixedAsset model) : base(model)
        {
        }

        public int Id => Model.Id;

        public string Name
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }

        public string Number
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }

        public int? BranchOfficeId
        {
            get { return GetValue<int?>(); }
            set { SetValue(value); }
        }

        protected override IEnumerable<string> ValidateErrors(string propertyName)
        {
            switch (propertyName)
            {
                case nameof(Name):
                    if (string.Equals(Name, "Test", StringComparison.OrdinalIgnoreCase))
                    {
                        yield return "Test - невалидное имя.";
                    }
                    break;
            }
        }
    }
}