﻿using System;
using System.Windows;
using System.Windows.Threading;
using Autofac;
using RegistrationMajorFacilities.UI.Startup;

namespace RegistrationMajorFacilities.UI
{
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Bootstrapper bootstrapper = new Bootstrapper();
            var container = bootstrapper.Bootstrap();

            var mainWindow = container.Resolve<MainWindow>();
            mainWindow.Show();
        }

        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Непредвиденная ошибка. Пожалуйста сообщите администратору."
                            + Environment.NewLine + e.Exception.Message, "Непредвиденная ошибка.");

            e.Handled = true;
        }
    }
}
