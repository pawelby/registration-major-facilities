﻿using Prism.Events;

namespace RegistrationMajorFacilities.UI.Events
{
    public class OpenFixedAssetDetailViewEvent : PubSubEvent<int?>
    {
    }
}
