﻿namespace RegistrationMajorFacilities.UI.Events.EventArgs
{
    public class AfterFixedAssetSavedEventArgs
    {
        public int Id { get; set; }

        public string DisplayMember { get; set; }
    }
}
