﻿using Prism.Events;
using RegistrationMajorFacilities.UI.Events.EventArgs;

namespace RegistrationMajorFacilities.UI.Events
{
    public class AfterFixedAssetSaveEvent : PubSubEvent<AfterFixedAssetSavedEventArgs>
    {
    }
}
