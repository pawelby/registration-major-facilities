﻿using Prism.Events;

namespace RegistrationMajorFacilities.UI.Events
{
    public class AfterFixedAssetDeletedEvent : PubSubEvent<int>
    {        
    }
}
