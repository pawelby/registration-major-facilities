﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RegistrationMajorFacilities.Model;

namespace RegistrationMajorFacilities.UI.Data.Repositories.Interfaces
{
    public interface IRegistrationRepository
    {
        Task<FixedAsset> GetByIdAsync(int fixedAssetId);

        Task SaveAsync();

        bool HasChanges();

        void Add(FixedAsset fixedAsset);

        void Remove(FixedAsset model);
    }
}