﻿using System;
using RegistrationMajorFacilities.Model;
using System.Data.Entity;
using System.Threading.Tasks;
using RegistrationMajorFacilities.DataAccess;
using RegistrationMajorFacilities.UI.Data.Repositories.Interfaces;

namespace RegistrationMajorFacilities.UI.Data.Repositories
{
    public class RegistrationRepository : IRegistrationRepository
    {
        private RegistrationMajorFacilitiesDbContext _context;

        public RegistrationRepository(RegistrationMajorFacilitiesDbContext context)
        {
            _context = context;
        }

        public void Add(FixedAsset fixedAsset)
        {
            _context.FixedAssets.Add(fixedAsset);
        }

        public async Task<FixedAsset> GetByIdAsync(int fixedAssetId)
        {
            return await _context.FixedAssets.SingleAsync(x => x.Id == fixedAssetId);
        }

        public bool HasChanges()
        {
            return _context.ChangeTracker.HasChanges();
        }

        public void Remove(FixedAsset model)
        {
            _context.FixedAssets.Remove(model);
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
