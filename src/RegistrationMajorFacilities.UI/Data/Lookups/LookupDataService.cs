﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using RegistrationMajorFacilities.DataAccess;
using RegistrationMajorFacilities.Model;
using RegistrationMajorFacilities.UI.Data.Lookups.Interfaces;

namespace RegistrationMajorFacilities.UI.Data.Lookups
{
    public class LookupDataService : IFixedAssetLookupDataService, IBranchOfficeLookupDataService
    {
        private Func<RegistrationMajorFacilitiesDbContext> _contextCreator;

        public LookupDataService(Func<RegistrationMajorFacilitiesDbContext> contextCreator)
        {
            _contextCreator = contextCreator;
        }

        public async Task<IEnumerable<LookupItem>> GetFixedAssetLookupAsync()
        {
            using (var context = _contextCreator())
            {
                return await context.FixedAssets.AsNoTracking()
                    .Select(
                        x => new LookupItem
                        {
                            Id = x.Id,
                            DisplayMember = x.Name
                        })
                    .ToListAsync();
            }
        }

        public async Task<IEnumerable<LookupItem>> GetBranchOfficeLookupAsync()
        {
            using (var context = _contextCreator())
            {
                return await context.BranchOffices.AsNoTracking()
                    .Select(
                        x => new LookupItem
                        {
                            Id = x.Id,
                            DisplayMember = x.Name
                        })
                    .ToListAsync();
            }
        }
    }
}
