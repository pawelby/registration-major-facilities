﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RegistrationMajorFacilities.Model;

namespace RegistrationMajorFacilities.UI.Data.Lookups
{
    public interface IBranchOfficeLookupDataService
    {
        Task<IEnumerable<LookupItem>> GetBranchOfficeLookupAsync();
    }
}