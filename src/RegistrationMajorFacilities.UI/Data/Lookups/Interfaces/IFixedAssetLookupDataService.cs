﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RegistrationMajorFacilities.Model;

namespace RegistrationMajorFacilities.UI.Data.Lookups.Interfaces
{
    public interface IFixedAssetLookupDataService
    {
        Task<IEnumerable<LookupItem>> GetFixedAssetLookupAsync();
    }
}